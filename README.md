# Academy's Cafe
this project goals is to implement basic oop concept.

## Definition
- Menu
- MenuManager
- CartItem
- CartManager
- Restaurant


### Menu
A struct for menu with 3 attributes(menuId, menuName, price)

### MenuManager
A Class for managing menu.
- array of menu
- init - fill the menu array with predefined menu 
- addMenu - insert new menu to the menu array. this also help initialization process
- getMenuIndexFor - fetch a Menu from the array
- printMenuBook - print the menu to the console
- printMenuHeader - print the header of the menu(called in printMenuBook)
- tidier - to tidyup the console printing by inserting whitespace

### CartItem
A struct for cart with 3 attributes(cartId, menu, qty)

### CartManager
A Class for managing cart. This class inherit MenuManager Class
- array of cartItem
- TotalPrice - to keep track total price upon change on cart
- currentId - help managing the id of cartItem
- buy - insert new cartItem into the cart with Menu & qty as input parameter
- getQty - ask the qty the user want to buy
- clearCart - reset the cart
- recalculateTotalPrice - update total price
- updateQty - update qty for a cartItem
- deleteCartItem - remove a cartItem from the array
- getFoodCount - return total food in the cart
- getBeverageCount - return total beverage in the cart
- printCart - print the cart
- printCartManagement - print the cart items and the menu to manage the cart
- manageCart - to edit or delete items in cart
- payment - start payment process, will reset cart upon success payment

### Restaurant
A Class to manage the restaurant. this class inherit CartManager Class.
- RestoName variable
- overriding printMenuHeader function in MenuManager to print custom menu header
- open - function to call to open the restaurant 
- printMenu - print the main menu 

## Flowchart
```mermaid
    graph TD
	Start((Start)) --> PrintMainMenu[Print Main Menu]
    PrintMainMenu --> Menu{Choose Menu}
    Menu --> |ViewCart|Menu2.1[Print Cart]
    Menu2.1 --> Menu
    Menu --> |BuyFood|Menu1.1[Print Menu book]
    Menu1.1 --> Menu1.2{Choose Menu}
    Menu1.2 --> |Food/Beverage|Menu1.3[Ask Qty]
    Menu1.3 --> Menu1.4[InsertToCart]
    Menu1.4 --> Menu
    Menu1.2 --> |Q|Menu
    Menu --> |Manage Crat|Menu3.1[Print Cart]
    Menu3.1 --> Menu3.2{Choose Menu}
    Menu3.2 -->|Update Qty|Menu3.3[Ask Item ID]
    Menu3.3 --> Menu3.7[Ask New Qty]
    Menu3.2 -->|Clear Cart|Menu3.5[Clear Cart]
    Menu3.5 --> Menu3.2
    Menu3.7 --> |Update Qty|Menu3.2
    Menu3.2 -->|Delete Item|Menu3.4[Ask Item Id]
    Menu3.4 --> Menu3.9[Delete Item]
    Menu3.9 --> Menu3.2
    Menu3.2 -->|Back To Main Menu|Menu
    Menu --> |Payment|Menu4.1[Print Bill]
    Menu4.1 --> Menu4.2[Input total paid money]
    Menu4.2 --> Menu4.3{Cek Paid Total}
    Menu4.3 --> |Paid < Bill|Menu4.4[Print Payment failed]
    Menu4.3 --> |Paid >= Bill|Menu4.5[Print Payment success]
    Menu4.5 --> Menu4.6[Print Invoice]
    Menu4.6 --> Menu4.7[Reset Cart]
    Menu4.7 --> Menu
    Menu --> |C|Stop[Close Restaurant]
```

## Created by - [Steven Wijaya](https://gitlab.com/swrhythm95)
- [Linkedin](https://www.lingkedin.com/in/stevenwijaya95)
- [Email](mailto:stevenwijaya2195@gmail.com?subject=Challenge%202@20-@20GitLab)

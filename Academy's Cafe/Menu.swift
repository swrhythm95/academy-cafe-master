import Foundation

struct Menu {
	var menuId: String
	var menuName: String
	var price: Int
}

import Foundation

struct CartItem {
	var cartId: Int
	var menu: Menu
	var qty: Int
}

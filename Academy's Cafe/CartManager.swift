import Foundation

class CartManager {
	var cartItem: [CartItem] = []
	var totalPrice: Int = 0
	var currentId: Int = 1
	
	func buy(parMenu: Menu, parQty: Int){
		let newCartItem: CartItem = CartItem(cartId: currentId, menu: parMenu, qty: parQty)
		cartItem.append(newCartItem)
		totalPrice += (newCartItem.qty * newCartItem.menu.price)
		currentId += 1
	}
	
	func clearCart(){
		cartItem.removeAll()
	}
	
	func recalculateTotalPrice(){
		totalPrice = 0
		
		for item in cartItem{
			totalPrice += (item.qty * item.menu.price)
		}
		
	}
	
	func updateQty(parCartId: Int, parNewQty: Int){
		var counter = 0
		var index = -1
		
		for item in cartItem {
			if item.cartId == parCartId {
				index = counter
			}
			counter += 1
		}
		
		if index == -1 {
			print("Item not found")
		}else{
			cartItem[index].qty = parNewQty
		}
	}
	
	func deleteCartItem(parCartId: Int){
		var temp: [CartItem] = []
		for item in cartItem {
			if item.cartId != parCartId {
				temp.append(item)
			}
		}
		cartItem = temp
		recalculateTotalPrice()
	}
	
	func getFoodCount() -> Int{
		var fc = 0
		for item in cartItem {
			if item.menu.menuId.first == "F" {
				fc += item.qty
			}
		}
		return fc
	}
	
	func getBeverageCount() -> Int{
		var bc = 0
		for item in cartItem {
			if item.menu.menuId.first == "B" {
				bc += item.qty
			}
		}
		return bc
	}
	
	func printCart(){
		let width:Int = 60
		if cartItem.count == 0 {
			print("\n\n\n\n\n")
			print(String(repeating: "=", count: width))
			print(tidier(string: "| 🛒Your cart is empty ‼️", length: width))
			print(String(repeating: "=", count: width))
		}else{
			print("\n\n\n\n\n")
			print(String(repeating: "=", count: width))
			print(tidier(string: "| 🛒 Hi, there're \(getFoodCount()) Food & \(getBeverageCount())Beverage in your cart.", length: width))
			print(String(repeating: "=", count: width))
			for item in cartItem {
				let menuName = item.menu.menuName.padding(toLength: 20, withPad: " ", startingAt: 0)
				let menuQty = String(item.qty).padding(toLength: 3, withPad: " ", startingAt: 0)
				
				var whiteSpaceNeeded = 8 - String(item.menu.price).count
				let menuprice = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(item.menu.price))"
				
				var rowTotal = String(item.qty*item.menu.price)
				whiteSpaceNeeded = 8 - rowTotal.count
				rowTotal = "\(String(repeating: " ", count: whiteSpaceNeeded))\(rowTotal)"
				print(tidier(string: "| \(menuName) \(menuQty) x \(menuprice) = \(rowTotal)", length: width))
			}
			let whiteSpaceNeeded = 8 - String(totalPrice).count
			let menuprice = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(totalPrice))"
			print(String(repeating: "-", count: width))
			print(tidier(string: "|\(String(repeating: " ", count: 31))Total = \(menuprice)", length: width))
			print(String(repeating: "=", count: width))
		}
	}
	func payment(){
		let width:Int = 60
		
		if cartItem.count == 0 {
			print("\n\n\n\n\n")
			print(String(repeating: "=", count: width))
			print(tidier(string: "| 💸 Payment Failed. No Item in cart", length: width))
			print(String(repeating: "=", count: width))
			print("Press enter to back to main menu ", terminator: "")
			_ = readLine()
		}else{
			print("Paid Amount: ", terminator: "")
			let Paid = Int(readLine()!)!
			if Paid < totalPrice{
				print("\n\n\n\n\n")
				print(String(repeating: "=", count: width))
				print(tidier(string: "| 💸 Payment Failed. money missing", length: width))
				print(String(repeating: "=", count: width))
				print("Press enter to back to main menu ", terminator: "")
				_ = readLine()
			}else{
				print("\n\n\n\n\n")
				print(String(repeating: "=", count: width))
				print(tidier(string: "| 💵 Invoice", length: width))
				print(String(repeating: "=", count: width))
				for item in cartItem {
					let menuName = item.menu.menuName.padding(toLength: 20, withPad: " ", startingAt: 0)
					let menuQty = String(item.qty).padding(toLength: 3, withPad: " ", startingAt: 0)
					var whiteSpaceNeeded = 8 - String(item.menu.price).count
					let menuprice = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(item.menu.price))"
					var rowTotal = String(item.qty*item.menu.price)
					whiteSpaceNeeded = 8 - rowTotal.count
					rowTotal = "\(String(repeating: " ", count: whiteSpaceNeeded))\(rowTotal)"
					print(tidier(string: "| \(menuName) \(menuQty) x \(menuprice) = \(rowTotal)", length: width))
				}
				var whiteSpaceNeeded = 8 - String(totalPrice).count
				let menuprice = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(totalPrice))"
				whiteSpaceNeeded = 8 - String(Paid).count
				let payment = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(Paid))"
				whiteSpaceNeeded = 8 - String(Paid-totalPrice).count
				let change = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(Paid-totalPrice))"
				print(String(repeating: "-", count: width))
				print(tidier(string: "|\(String(repeating: " ", count: 31))Total = \(menuprice)", length: width))
				print(tidier(string: "|\(String(repeating: " ", count: 32))Paid = \(payment)", length: width))
				print(tidier(string: "|\(String(repeating: " ", count: 30))Change = \(change)", length: width))
				print(String(repeating: "=", count: width))
				clearCart()
				print("Press enter to back to main menu ", terminator: "")
				_ = readLine()
			}
		}
	}
	
	func tidier(string: String, length: Int) -> String {
		var lineFinal = string
		lineFinal = lineFinal.padding(toLength: length-1, withPad: " ", startingAt: 0)
		lineFinal.append(lineFinal.first!)
		
		return lineFinal
	}
	
	func getQty(menuName:String)->Int
	{
		print("How many \"\(menuName)\" you want to buy? " , terminator: "")
		let userInputQty = readLine()
		let checker = CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: userInputQty!))
		if checker == false{
			return -1
		}else{
			return Int(userInputQty!)!
		}
	}
	
	func manageCart(){
		var choosenMenu = "0"
		
		repeat{
			printCartManagement()
			if cartItem.count != 0 {
				print("Choose Menu? ", terminator: "")
				choosenMenu = readLine()!.uppercased()
				switch choosenMenu {
				case "1":
					if choosenMenu != "Q"{
						print("Item Id : ", terminator: "")
						choosenMenu = readLine()!.uppercased()
						print("New Qty : ", terminator: "")
						let qty = Int(readLine()!)!
						
						if qty > 0 {
							updateQty(parCartId: Int(choosenMenu)!, parNewQty: qty)
						}else if qty == 0 {
							deleteCartItem(parCartId: Int(choosenMenu)!)
						}else{
							print("Insert valid number!")
						}
					}
				case "2":
					if choosenMenu != "Q"{
						print("Item Id : ", terminator: "")
						choosenMenu = readLine()!.uppercased()
						deleteCartItem(parCartId: Int(choosenMenu)!)
					}
				case "3":
					clearCart()
				default:
					print("Menu not valid")
				}
			}else{
				choosenMenu = "Q"
				print("Press enter to back to main menu ", terminator: "")
				_ = readLine()
			}
		}while(choosenMenu != "Q")
	}
	
	func printCartManagement(){
		let width:Int = 60
		print("\n\n\n\n\n")
		if cartItem.count == 0 {
			print("\n\n\n\n\n")
			print(String(repeating: "=", count: width))
			print(tidier(string: "| 🛒Your cart is empty ‼️", length: width))
			print(String(repeating: "=", count: width))
		}else{
			print("\n\n\n\n\n")
			print(String(repeating: "=", count: width))
			print(tidier(string: "| Current Cart", length: width))
			print(String(repeating: "=", count: width))
			for item in cartItem {
				let itemId = String(item.cartId).padding(toLength: 4, withPad: " ", startingAt: 0)
				let menuName = item.menu.menuName.padding(toLength: 20, withPad: " ", startingAt: 0)
				let menuQty = String(item.qty).padding(toLength: 3, withPad: " ", startingAt: 0)
				var whiteSpaceNeeded = 8 - String(item.menu.price).count
				let menuprice = "\(String(repeating: " ", count: whiteSpaceNeeded))\(String(item.menu.price))"
				var rowTotal = String(item.qty*item.menu.price)
				whiteSpaceNeeded = 8 - rowTotal.count
				rowTotal = "\(String(repeating: " ", count: whiteSpaceNeeded))\(rowTotal)"
				print(tidier(string: "| [id: \(itemId)] \(menuName) \(menuQty) x \(menuprice) = \(rowTotal)", length: width))
			}
			print(String(repeating: "-", count: width))
			print(tidier(string: "| Menu:", length: width))
			print(tidier(string: "| [1] Update Qty", length: width))
			print(tidier(string: "| [2] Delete", length: width))
			print(tidier(string: "| [3] Clear Cart", length: width))
			print(tidier(string: "| [Q] Back to Main menu", length: width))
			print(String(repeating: "=", count: width))
		}
	}
}

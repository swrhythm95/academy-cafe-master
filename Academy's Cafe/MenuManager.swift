import Foundation

class MenuManager{
	var menuList: [Menu] = []
	var width: Int = 60
	
	init() {
		addMenu(parMenuId: "F01", parMenuName: "Gado-Gado", parPrice: 17000)
		addMenu(parMenuId: "F02", parMenuName: "Rawon", parPrice: 25000)
		addMenu(parMenuId: "F03", parMenuName: "Nasi goreng", parPrice: 22000)
		addMenu(parMenuId: "B01", parMenuName: "Air Mineral", parPrice: 4000)
		addMenu(parMenuId: "B02", parMenuName: "Es Teh", parPrice: 6000)
	}
	
	func addMenu(parMenuId: String, parMenuName: String, parPrice: Int){
		var idAvailable: Bool = true
		
		for cek in menuList {
			if parMenuId == cek.menuId {
				idAvailable = false
			}
		}
		
		if idAvailable {
			let newMenu = Menu(menuId: parMenuId, menuName: parMenuName, price: parPrice)
			menuList.append(newMenu)
		}else{
			print("Menu Id already used for other menu!")
		}
	}
	
	func getMenuIndexFor(parMenuId: String) -> Int{
		return menuList.firstIndex { $0.menuId == parMenuId } ?? -1
	}
	
	func printMenuBook(){
		printMenuHeader()
		
		for item in menuList {
			print(tidier(string: "| [\(item.menuId)] \(item.menuName)", length: width))
		}
		
		print(tidier(string: "| [ Q ] Back to main menu", length: width))
		print(String(repeating: "-", count: width))
	}
	
	func printMenuHeader() {
		print("\n\n\n\n\n")
		print("------------------------------------------------------------")
		print("| Welcome, We have \(menuList.count) F & B option for you! |")
		print("------------------------------------------------------------")
	}
	
	func tidier(string: String, length: Int) -> String {
		var lineFinal = string
		lineFinal = lineFinal.padding(toLength: length-1, withPad: " ", startingAt: 0)
		lineFinal.append(lineFinal.first!)
		
		return lineFinal
	}
}

import Foundation

class Restaurant: MenuManager {
	var cartManager: CartManager = CartManager()
	var choosenMenu: String = ""
	var qty: Int = 0
	var RestoName: String = "Apple Resto"
	
	override func printMenuHeader() {
		let titleLength: String = "\(RestoName)'s Menu"
		print("\n\n\n\n\n")
		print(String(repeating: "-", count: width))
		print("|\(String(repeating: " ", count: (width-2-titleLength.count)/2))\(titleLength)\(String(repeating: " ", count: (width-2-titleLength.count)/2))|")
		print(String(repeating: "-", count: width))
	}
	
	func open(){
		repeat{
			printMenu()
			choosenMenu = readLine()!.uppercased()
			switch choosenMenu {
			case "1":
				repeat{
					printMenuBook()
					print("Choose item? ", terminator: "")
					choosenMenu = readLine()!.uppercased()
					if getMenuIndexFor(parMenuId: choosenMenu) != -1 {
						if choosenMenu != "Q"{
							qty = cartManager.getQty(menuName: menuList[getMenuIndexFor(parMenuId: choosenMenu)].menuName)
							if qty > 0 {
								cartManager.buy(parMenu: menuList[getMenuIndexFor(parMenuId: choosenMenu)], parQty: qty)
							}else if qty == 0 {
								print("You can't buy in 0 Qty!")
							}else{
								print("Insert valid number!")
							}
						}
					}
				}while(choosenMenu != "Q")
			case "2":
				cartManager.printCart()
				print("Press enter to back to main menu ", terminator: "")
				_ = readLine()
			case "3":
				cartManager.manageCart()
			case "4":
				cartManager.printCart()
				cartManager.payment()
			case "C":
				choosenMenu = "C"
			default:
				print("we don't have that menu")
			}
		}while(choosenMenu != "C")
		print("Restaurant Closed!")
	}
	
	func printMenu() {
		print("""
    \n\n\n\n\n
    =================================
    ┃  Academy's Cafe & Resto v2.0  ┃
    =================================
    ┃                               ┃
    ┃ Options:                      ┃
    ┃ [1] Buy Food                  ┃
    ┃ [2] Print Cart                ┃
    ┃ [3] Manage Cart               ┃
    ┃ [4] Make Payment              ┃
    ┃ [C] Close Restaurant          ┃
    ┃                               ┃
    =================================
    """)
		print("Your choice? ", terminator: "")
	}
}
